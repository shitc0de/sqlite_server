#ifndef QUERYCRAFTER_H
#define QUERYCRAFTER_H

#include <QWidget>

namespace Ui {
class QueryCrafter;
}

class QueryCrafter : public QWidget
{
    Q_OBJECT

public:
    explicit QueryCrafter(QWidget *parent = nullptr);
    ~QueryCrafter();
public slots:
    void setDefaultQuery(const QString& query);
    void setResponse(const QString& response);
    void closeEvent(QCloseEvent *event) override;
signals:
    void executeThis(const QString&);
    void shouldClose();
private slots:
    void on_clearResponseBtn_clicked();
    void on_executeBtn_clicked();
    void on_closeBtn_clicked();

private:
    Ui::QueryCrafter *ui;
};

#endif // QUERYCRAFTER_H
