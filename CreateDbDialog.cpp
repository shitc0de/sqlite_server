#include "CreateDbDialog.h"
#include "ui_CreateDbDialog.h"

DBColumn_local::DBColumn_local()
{
    notNull = false;
    type = NONE;
    primaryKey = false;
    autoIncrement = false;
    m_name = QString();
}

CreateDbDialog::CreateDbDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateDbDialog)
{
    ui->setupUi(this);
    setTableHeader();
    connect(ui->dbTables,&QListWidget::itemClicked,this,&CreateDbDialog::updateTable);
    connect(ui->dbColumns, &QTableWidget::cellChanged,[&](int row, int col) {
        auto it = ui->dbColumns->item(row,col);
        emit updateRecords(it);
    });
}

void CreateDbDialog::extractData(QString &query, QString &dbPath)
{
    dbPath = m_dbPath;
    query.clear();
    for (auto &table:m_tables) {
        query += "CREATE TABLE " + table.first +"(\n";
        for (auto &col:table.second) {
            query += col.m_name;
            switch (col.type) {
            case DBColumn_local::NONE:
                query += "";
                break;
            case DBColumn_local::INTEGER:
                query += " INTEGER";
                break;
            case DBColumn_local::REAL:
                query += " REAL";
                break;
            case DBColumn_local::TEXT:
                query += " TEXT";
                break;
            case DBColumn_local::NUMERIC:
                query += " NUMERIC";
            default: assert(0);
            }
            if (col.primaryKey) query += " PRIMARY KEY";
            if (col.notNull) query += " NOT NULL";
            if (col.autoIncrement) query += " AUTOINCREMENT";
            query += ",\n";
        }
        query.chop(2);
        query += "\n";
        query +=");\n";
    }
}

void CreateDbDialog::closeEvent(QCloseEvent *e)
{
    if (ui->dbTables->count() == 0 || QMessageBox::Yes == QMessageBox::warning(this,QString("Really?"),QString("Data will be lost"),
        QMessageBox::StandardButton::No|QMessageBox::StandardButton::Yes,QMessageBox::StandardButton::No)) {
        e->accept();
    }
    else e->ignore();
}

CreateDbDialog::~CreateDbDialog()
{
    delete ui;
}

void CreateDbDialog::on_dbPathBtn_clicked()
{
    QFileDialog dlg;
    dlg.setFileMode(QFileDialog::FileMode::AnyFile);
    m_dbPath = dlg.getSaveFileName(nullptr, QString("Enter new db name"), QDir::currentPath(), QString("DB (*.db)"));
    ui->dbPathEdit->setText(m_dbPath);
}

void CreateDbDialog::on_newTableBtn_clicked()
{
    QString tname = QInputDialog::getText(this,QString("Table name"),QString("Enter new table name"));
    QRegularExpression re("[a-zA-Z0-9_-]+");
    if (re.match(tname).captured().length() == 0 || re.match(tname).captured().length() != tname.length()) {
        QMessageBox::critical(this,QString("Error"),QString("Wrong table name.\nTable name should contain latin symbols, digits and '-','_' symbols"));
        return;
    }
    TableRecord rec;
    rec.first = tname;
    m_tables.push_back(rec);
    updateList();
    ui->dbTables->setCurrentRow(ui->dbTables->count()-1);

}

void CreateDbDialog::on_delTableBtn_clicked()
{
    if (ui->dbTables->currentRow()<0) return;
    else {
        m_tables.erase(m_tables.begin() + ui->dbTables->currentRow());
        updateList();
        ui->dbColumns->clear();
        ui->dbColumns->setRowCount(0);
        setTableHeader();
    }
}

void CreateDbDialog::on_newColBtn_clicked()
{
    if (ui->dbTables->currentRow()<0) return;
    else {
        m_tables[ui->dbTables->currentRow()].second.push_back(DBColumn_local());
        emit updateTable(ui->dbTables->currentItem());
    }
}

void CreateDbDialog::on_delColBtn_clicked()
{
    if (ui->dbTables->currentRow()<0) return;
    else {
        int row = ui->dbColumns->currentRow();
        if (row < 0) return;
        m_tables[ui->dbTables->currentRow()].second.erase(m_tables[ui->dbTables->currentRow()].second.begin()+row);
        emit updateTable(ui->dbTables->currentItem());
    }
}

void CreateDbDialog::updateList()
{
    ui->dbTables->clear();
    for (const auto &table : m_tables) {
        ui->dbTables->insertItem(ui->dbTables->count(),new QListWidgetItem(table.first));
    }
}

void CreateDbDialog::updateTable(QListWidgetItem* item)
{
    int tableSelection = item->listWidget()->currentRow();
    assert(tableSelection>=0 && tableSelection < ui->dbTables->count());
    ui->dbColumns->clear();
    ui->dbColumns->setRowCount(0);
    setTableHeader();
    for (auto& tableColumn : m_tables[tableSelection].second) {
        ui->dbColumns->insertRow(ui->dbColumns->rowCount());
        ui->dbColumns->setItem(ui->dbColumns->rowCount()-1,0,new QTableWidgetItem(tableColumn.m_name));
        auto typeMenu = new QMenu(this);
        typeMenu->addAction("NONE");
        typeMenu->addAction("INTEGER");
        typeMenu->addAction("REAL");
        typeMenu->addAction("TEXT");
        typeMenu->addAction("NUMERIC");
        QPushButton* btn = new QPushButton(this);
        switch (tableColumn.type) {
        case DBColumn_local::NONE:
            btn->setText("NONE");
            break;
        case DBColumn_local::INTEGER:
            btn->setText("INTEGER");
            break;
        case DBColumn_local::REAL:
            btn->setText("REAL");
            break;
        case DBColumn_local::TEXT:
            btn->setText("TEXT");
            break;
        case DBColumn_local::NUMERIC:
            btn->setText("NUMERIC");
            break;
        default:
            assert(false);
        }
        btn->setMenu(typeMenu);
        connect(typeMenu,&QMenu::triggered,[&](QAction* act){
            QString t = act->text();
            qobject_cast<QPushButton*>(ui->dbColumns->cellWidget(ui->dbColumns->currentRow(),ui->dbColumns->currentColumn()))->setText(t);
            emit ui->dbColumns->cellChanged(ui->dbColumns->currentRow(),ui->dbColumns->currentColumn());
        });
        ui->dbColumns->setItem(ui->dbColumns->rowCount()-1,1,new QTableWidgetItem());
        ui->dbColumns->setCellWidget(ui->dbColumns->rowCount()-1,1,btn);
        auto chkNull = new QCheckBox(this);
        chkNull->setCheckState(tableColumn.notNull ? Qt::Checked : Qt::Unchecked);
        ui->dbColumns->setItem(ui->dbColumns->rowCount()-1,2,new QTableWidgetItem());
        ui->dbColumns->setCellWidget(ui->dbColumns->rowCount()-1,2,chkNull);
        connect(chkNull,&QCheckBox::stateChanged,[&](){
            emit ui->dbColumns->cellChanged(ui->dbColumns->currentRow(),ui->dbColumns->currentColumn());
        });
        auto chkPrimary = new QCheckBox(this);
        chkPrimary->setCheckState(tableColumn.primaryKey ? Qt::Checked : Qt::Unchecked);
        connect(chkPrimary,&QCheckBox::stateChanged,this,&CreateDbDialog::checkboxPrimaryKeyChanged);
        ui->dbColumns->setItem(ui->dbColumns->rowCount()-1,3,new QTableWidgetItem());
        ui->dbColumns->setCellWidget(ui->dbColumns->rowCount()-1,3,chkPrimary);
        connect(chkPrimary,&QCheckBox::stateChanged,[&](){
            emit ui->dbColumns->cellChanged(ui->dbColumns->currentRow(),ui->dbColumns->currentColumn());
        });
        auto chkIncrement = new QCheckBox(this);
        chkIncrement->setCheckState(tableColumn.autoIncrement ? Qt::Checked : Qt::Unchecked);
        ui->dbColumns->setItem(ui->dbColumns->rowCount()-1,4,new QTableWidgetItem());
        ui->dbColumns->setCellWidget(ui->dbColumns->rowCount()-1,4,chkIncrement);
        connect(chkIncrement,&QCheckBox::stateChanged,[&](){
            emit ui->dbColumns->cellChanged(ui->dbColumns->currentRow(),ui->dbColumns->currentColumn());
        });
    }
}

void CreateDbDialog::setTableHeader()
{
    const QStringList labels{"Column name", "Column type", "NOT NULL", "Primary key", "Auto increment"};
    ui->dbColumns->setHorizontalHeaderLabels(labels);
}

void CreateDbDialog::checkboxPrimaryKeyChanged(int newState)
{
    auto chk = qobject_cast<QCheckBox*>(sender());
    if (chk->checkState() == Qt::Unchecked) return;
    else {
        for (int i = 0; i < ui->dbColumns->rowCount();++i) {
            if (chk == qobject_cast<QCheckBox*>(ui->dbColumns->cellWidget(i,3))) continue;
            else {
                (qobject_cast<QCheckBox*>(ui->dbColumns->cellWidget(i,3)))->setCheckState(Qt::Unchecked);
                m_tables[ui->dbTables->currentRow()].second[i].primaryKey = false;
            }
        }
    }
}

void CreateDbDialog::updateRecords(QTableWidgetItem* item)
{
    int row = item->row();
    int col = item->column();
    auto& it = m_tables[ui->dbTables->currentRow()].second[row];
    auto tableItem = ui->dbColumns->cellWidget(row,col);
    if (col > 0 && tableItem == nullptr) return;
    switch (col) {
    case 0: {
        it.m_name = item->text();
        break;
    }
    case 1: {
        auto btnText = qobject_cast<QPushButton*>(tableItem)->text();
        if (btnText == nullptr) return;
        if (btnText == "NONE") it.type = DBColumn_local::NONE;
        else if (btnText == "INTEGER") it.type = DBColumn_local::INTEGER;
        else if (btnText == "REAL") it.type = DBColumn_local::REAL;
        else if (btnText == "TEXT") it.type = DBColumn_local::TEXT;
        else if (btnText == "NUMERIC") it.type = DBColumn_local::NUMERIC;
        else assert(0);
        break;
    }
    case 2: {
        bool chk = (qobject_cast<QCheckBox*>(tableItem)->checkState() == Qt::Checked);
        it.notNull = chk;
        break;
    }
    case 3: {
        bool chk = (qobject_cast<QCheckBox*>(tableItem)->checkState() == Qt::Checked);
        it.primaryKey = chk;
        break;
    }
    case 4: {
        bool chk = (qobject_cast<QCheckBox*>(tableItem)->checkState() == Qt::Checked);
        it.autoIncrement = chk;
        break;
    }
    default:
        assert(0);
    }
}

void CreateDbDialog::on_moveUpBtn_clicked()
{
    int currentCol = ui->dbColumns->currentColumn();
    int currentRow = ui->dbColumns->currentRow();
    if (ui->dbTables->currentRow() < 0 || ui->dbColumns->currentRow() < 1) return;
    std::swap(m_tables[ui->dbTables->currentRow()].second[ui->dbColumns->currentRow()],
            m_tables[ui->dbTables->currentRow()].second[ui->dbColumns->currentRow()-1]);
    emit updateTable(ui->dbTables->currentItem());
    ui->dbColumns->setCurrentCell(currentRow-1, currentCol);
}

void CreateDbDialog::on_moveDownBtn_clicked()
{
    int currentCol = ui->dbColumns->currentColumn();
    int currentRow = ui->dbColumns->currentRow();
    if (ui->dbColumns->currentRow() < 0 ||
            ui->dbColumns->currentRow() == ui->dbColumns->rowCount()-1) return;
    std::swap(m_tables[ui->dbTables->currentRow()].second[ui->dbColumns->currentRow()],
            m_tables[ui->dbTables->currentRow()].second[ui->dbColumns->currentRow()+1]);
    emit updateTable(ui->dbTables->currentItem());
    ui->dbColumns->setCurrentCell(currentRow+1, currentCol);
}

void CreateDbDialog::on_acceptBtn_clicked()
{
    if (m_dbPath.isEmpty()) {
        QMessageBox::critical(this,QString("Missing db path and name"),QString("Please, select db directory and name"));
        return;
    }
    return QDialog::accept();
}

void CreateDbDialog::on_cancelBtn_clicked()
{
    if (ui->dbTables->count() == 0 || QMessageBox::Yes == QMessageBox::warning(this,QString("Really?"),QString("Data will be lost"),
        QMessageBox::StandardButton::No|QMessageBox::StandardButton::Yes,QMessageBox::StandardButton::No)) {
        QDialog::reject();
    }
}
