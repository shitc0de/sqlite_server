#include "DBWidget.h"
#include "ui_DBWidget.h"

DBWidget::DBWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DBWidget)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/ico/shrimp.png"));
    m_updateInterval = 2000;
    connect(ui->tablesList,&QListWidget::itemClicked,this,&DBWidget::fillTable);
}

void DBWidget::closeEvent(QCloseEvent *event)
{
    emit shouldClose();
    event->accept();
}

DBWidget::~DBWidget()
{
    delete ui;
    delete m_updater;
}

void DBWidget::setUpdateInterval(int ms)
{
    m_updateInterval = ms;
}

void DBWidget::attachDB(DBAsync *_db)
{
    db = _db;
    ui->dbNameLabel->setText(db->dbName());
    m_updater = new QTimer(this);
    m_updater->setInterval(m_updateInterval);
    m_updater->setSingleShot(false);
    connect(m_updater,&QTimer::timeout,this,&DBWidget::fillTables);
    m_updater->start();
}

void DBWidget::fillTables()
{
    //get tables list
    m_tables.clear();
    QString tablesRaw = db->inplaceExecute(m_getTablesQuery);
    QRegularExpression re(m_getTablesRegex);
    auto matches = re.globalMatch(tablesRaw);
    QList<QString> tablesList;
    while (matches.hasNext()) {
        auto match = matches.next();
        tablesList.push_back(match.captured(1));
    }
    int _r = ui->tablesList->currentRow();
    ui->tablesList->clear();
    for (const auto& table : tablesList) {
        ui->tablesList->insertItem(ui->tablesList->count(),table);
    }
    ui->tablesList->setCurrentRow(_r);
    for (int i = 0; i < tablesList.size(); ++i) {
        //get columns from table
        re.setPattern(m_getTablesRegex);
        TableData t;
        t.tname = tablesList[i];
        QString query = "PRAGMA table_info(" + tablesList[i] + ")";
        QString headers = db->inplaceExecute(query);
        matches = re.globalMatch(headers);
        while (matches.hasNext()) {
            auto match = matches.next();
            t.theaders.push_back(match.captured(1));
        }
        //get table data
        query = "SELECT * FROM " + tablesList[i];
        QString tdata = db->inplaceExecute(query);
        re.setPattern("(.*)=(.*)");
        matches = re.globalMatch(tdata);
        int fieldId = 0;
        QList<QString> rowData;
        while (matches.hasNext()) {
            auto match = matches.next();
            rowData.push_back(match.captured(2));
            fieldId++;
            if (fieldId == t.theaders.size()) {
                t.tdata.push_back(rowData);
                rowData.clear();
                fieldId = 0;
            }
        }
        m_tables.push_back(std::move(t));
    }
    fillTable(ui->tablesList->currentItem());
}

void DBWidget::fillTable(QListWidgetItem *it)
{
    if (it == nullptr) {
        ui->tableData->clear();
        ui->tableData->setRowCount(0);
        ui->tableData->setColumnCount(0);
        return;
    }
    int rowId = it->listWidget()->currentRow();
    if (rowId < 0) return;
    int _r = ui->tableData->currentRow();
    ui->tableData->clear();
    ui->tableData->setRowCount(0);
    ui->tableData->setColumnCount(m_tables[rowId].theaders.size());
    ui->tableData->setHorizontalHeaderLabels(m_tables[rowId].theaders);
    for (int i = 0; i < m_tables[rowId].tdata.size(); ++i) {
        int row = ui->tableData->rowCount();
        ui->tableData->insertRow(row);
        for (int j = 0; j < m_tables[rowId].tdata[i].size(); ++j) {
            ui->tableData->setItem(i, j, new QTableWidgetItem(m_tables[rowId].tdata[i][j]));
        }
    }
    ui->tableData->setEditTriggers(QTableWidget::NoEditTriggers);
    ui->tableData->setSelectionBehavior(QAbstractItemView::SelectRows);
    if (_r >= 0 && _r < ui->tableData->rowCount()) ui->tableData->setCurrentItem(ui->tableData->item(_r,0));

}
