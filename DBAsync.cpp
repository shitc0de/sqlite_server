#include "DBAsync.h"



DBAsync::DBAsync(QObject *parent) : QObject(parent)
{
    m_port = 8765;
}

QString DBAsync::inplaceExecute(const QString &query)
{
    return atomicExecute(query);
}

DBAsync::~DBAsync()
{
    stop();
}

bool DBAsync::openDB(const QString &dbname)
{
    m_dbName = dbname;
    m_sql.reset(new SQLite_Wrapper);
    if (m_dbName.isEmpty() || !m_sql->openDB(m_dbName)) {
        emit sendLog(QString("Can't open DB ") + m_dbName);
        m_sql.reset();
        return false;
    }
    emit dbOpened();
    emit sendLog(QString("Opened DB ") + dbName());
    return true;
}

void DBAsync::closeDB()
{
    if (!m_sql.isNull()) {
        stop();
        m_sql->closeDB();
        emit dbClosed();
    }
}

void DBAsync::setPort(int port)
{
    m_port = port;
}

bool DBAsync::run()
{
    if (m_sql.isNull()) {
        emit sendLog(QString("Can't start server w/o db. Open db first"));
        return false;
    }
    m_server.reset(new QTcpServer(this), &QTcpServer::close);
    if (!m_server->listen(QHostAddress::Any,m_port)) {
        emit sendLog(QString("Can't start server"));
    }
    else emit sendLog(QString("Server started at port ") + QString::number(m_port));
    connect(m_server.data(), &QTcpServer::newConnection, this, &DBAsync::slotNewConnection);
    return true;
}

void DBAsync::localExecute(const QString & query)
{
    QString resp = atomicExecute(query);
    emit sendLocalResponse(resp);
}

void DBAsync::stop()
{
    for (auto &i:m_sockets) {
        i->close();
    }
    m_sockets.clear();
    if (!m_server.isNull()) m_server->close();
}

QString DBAsync::dbName() const
{
    QRegularExpression re("([a-zA-Z0-9_ ]+)\\.db");
    return  re.match(m_dbName).captured(1);
}

void DBAsync::slotNewConnection()
{
    auto _socket = m_server->nextPendingConnection();
    connect(_socket, &QTcpSocket::disconnected, [&]() {
       m_sockets.remove(_socket);
    });
    connect(_socket,&QTcpSocket::readyRead,this,&DBAsync::slotServerRead);
    m_sockets.insert(_socket);
    emit sendLog(QString("Client ") + _socket->peerAddress().toString() + QString(" connected on port ") + QString::number(_socket->peerPort()));

}

void DBAsync::slotServerRead()
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    QByteArray array;
    if (socket) {
        while (socket->bytesAvailable() > 0) {
            array.append(QString::fromUtf8(socket->readAll()));
            emit sendLog(array);
        }
        auto res = atomicExecute(QString(array).toUtf8());
        if (res.isEmpty()) res = "Ok";
        emit sendLog(QString("New query: ") + array);
        socket->write(res.toUtf8());
    }
}

QString DBAsync::atomicExecute(const QString &query)
{
    std::lock_guard<std::mutex> _lg(m_mutex);
    QString ret;
    int rc = m_sql->execute(query);
    if (rc != SQLITE_OK) {
        ret = m_sql->getLastError();
    }
    else ret = m_sql->executeResult();
    return ret;
}
