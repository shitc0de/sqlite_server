#include "SQLite_Wrapper.h"

SQLite_Wrapper::SQLite_Wrapper(QObject *parent) :QObject(parent)
{
    m_lastError = QString("No error");
}

SQLite_Wrapper::~SQLite_Wrapper()
{
    closeDB();
}

QString SQLite_Wrapper::getLastError()
{
    return m_lastError;
}

QString SQLite_Wrapper::executeResult()
{
    if (m_lastError == QString("No error") && m_callbackResult.isEmpty()) {
        m_callbackResult = QString("OK");
    }
//    m_callbackResult = QString::fromUtf8(m_callbackResult);
    return std::move(m_callbackResult);
}

bool SQLite_Wrapper::openDB(const QString &dbName)
{
    m_DBName = dbName;
    sqlite3* db;
    int rc = sqlite3_open(dbName.toLocal8Bit().constData(),&db);
    if (rc) {
        m_lastError = QString("Can't open database ") + dbName;
        m_DBName.clear();
        return false;
    }
    m_DBPtr.reset(db,sqlite3_close);
    return true;
}

void SQLite_Wrapper::closeDB()
{
    if (!m_DBPtr.isNull()) {
        sqlite3_close(m_DBPtr.data());
        m_DBPtr.reset();
    }
}

void SQLite_Wrapper::writeToCallback(const QString& data)
{
    m_callbackResult.append(data);
}

int SQLite_Wrapper::executeCallback(void* _thisInstance, int argc, char** argv, char** azColName)
{
    if (SQLite_Wrapper* wInstance = static_cast<SQLite_Wrapper*>(_thisInstance)) {
        QString _data;
        for (int i = 0; i < argc; ++i) {
            _data += QString(azColName[i])+"=";
            _data += (argv[i] ? QString(argv[i]) : QString("NULL")) + QString('\n');
        }
        wInstance->writeToCallback(_data);
    }
    return 0;
}

sqlite3 *SQLite_Wrapper::getRawPtr()
{
    return m_DBPtr.data();
}

int SQLite_Wrapper::execute(const QString & query)
{
    char* errMsg = nullptr;
    QString cbRes;
    int rc = sqlite3_exec(m_DBPtr.data(),query.toUtf8().constData(),SQLite_Wrapper::executeCallback,reinterpret_cast<void*>(this),&errMsg);
    if (rc != SQLITE_OK) {
        m_lastError.clear();
        m_lastError.sprintf("SQL error: %s\n",errMsg);
        sqlite3_free(errMsg);
        return rc;
    }
    return rc;
}
