#ifndef SQLITE_WRAPPER_H
#define SQLITE_WRAPPER_H

#include <QObject>
#include <QString>
#include <QSharedPointer>
#include "sqlite3.h"

class SQLite_Wrapper : public QObject
{
    Q_OBJECT
public:
    explicit SQLite_Wrapper(QObject* parent = nullptr);
    ~SQLite_Wrapper();
    QString getLastError();
    QString executeResult();
    sqlite3* getRawPtr();
    int execute(const QString&);
public slots:
    bool openDB(const QString&);
    void closeDB();
    void writeToCallback(const QString&);
private:
    QString m_DBName;
    QString m_callbackResult;
    QSharedPointer<sqlite3> m_DBPtr;
    QString m_lastError;
    static int executeCallback(void*, int, char**, char**);
private slots:
};


#endif // SQLITE_WRAPPER_H
