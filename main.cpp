#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QLocale cLocale(QLocale("ru_RU"));
    QLocale::setDefault(cLocale);
    MainWindow w;
    w.show();
    return a.exec();
}
