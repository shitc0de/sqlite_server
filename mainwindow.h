#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMdiSubWindow>
#include "DBAsync.h"
#include "CreateDbDialog.h"
#include "QueryCrafter.h"
#include "DBWidget.h"
#include <QDebug>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    DBAsync* db;
    DBWidget* dbWidget;
    QMdiSubWindow* dbWidget_subw;
    QueryCrafter* crafter;
    QMdiSubWindow* crafter_subw;
private slots:
    void showCreateDbDialog();
    void showOpenDbDialog();
    void showCrafter();
    void onStartServer();
    void onStopServer();
};
#endif // MAINWINDOW_H
