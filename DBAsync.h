#ifndef DBASYNC_H
#define DBASYNC_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QRegularExpression>

#include "SQLite_Wrapper.h"

#include <mutex>

class DBAsync : public QObject
{
    Q_OBJECT
public:
    explicit DBAsync(QObject *parent = nullptr);
    QString inplaceExecute(const QString& query);
    ~DBAsync();
public slots:
    bool openDB(const QString&);
    void closeDB();
    void setPort(int);
    bool run();
    void localExecute(const QString&);
    void stop();
    QString dbName() const;
private slots:
    void slotNewConnection();
    void slotServerRead();
signals:
    void sendLog(const QString&);
    void sendLocalResponse(const QString&);
    void dbOpened();
    void dbClosed();
private:
    int m_port;
    QSharedPointer<SQLite_Wrapper> m_sql;
    QSharedPointer<QTcpServer> m_server;
    QSet<QTcpSocket*> m_sockets;
    std::mutex m_mutex;
    QString m_dbName;
    QString atomicExecute(const QString&);
};

#endif // DBASYNC_H
